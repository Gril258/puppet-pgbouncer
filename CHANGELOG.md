## 2017-12-22
### Summary
Added a new parameter

## Changed
  - Added a paramater for setting the timeout of database pool connections.

## 2016-06-14
### Summary
Fixed an issue with the pid file.

## 2016-01-26 Release 0.1.9, 0.1.8, 0.1.7, 0.1.6
### Summary
Support for Centos

### Changed
  - Added support for centos
  - Added user owner and group to userlist.txt
  - Added vagrant directory for testing
  - Updated Rakefile for automated testing (lint)
  - Added a params class which has OS dependant configuration.

## 2015-06-21 Release 0.1.5, 0.1.4
### Summary
Updated documentation

## 2015-06-21 Release 0.1.3
### Summary
Merge

### Changed
 - Merged community changes to clean up doco
 - Moved contributors to a text file

## 2015-03-04 Release 0.1.2
### Summary
Account Migration

### Changed
 - Migrated from github to bitbucket
 - Changed ownership of puppetforge account
 - Change log migrated to markdown.

## 2015-02-20 Version 0.1.1
### Summary
Added additional parameter

### Changes
 - Added openhub badge to readme.
 - Added pool_mode
 - Added auth_type

## 2015-02-19 Version 0.1.0
### Summary
Initial Release.
